// THESE BENCHMARKS REQUIRE A REDIS SERVER TO ALREADY BE RUNNING

use criterion::BenchmarkId;
use criterion::{criterion_group, criterion_main, Criterion};
use nanoid::nanoid;

use tokio::runtime::Runtime;

use redis::AsyncCommands;
use redis::RedisResult;

struct KV {
    k: String,
    v: String,
}

struct Wrapper(Vec<KV>);

impl std::fmt::Display for Wrapper {
    fn fmt(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        //write!(f, "{}", self.0)?;
        Ok(())
    }
}

async fn big_set(kvs: &Wrapper) {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_async_connection().await.unwrap();
    for kv in kvs.0.iter() {
        let _: RedisResult<String> = con.set(kv.k.clone(), kv.v.clone()).await;
    }
}

async fn big_get(kvs: &Wrapper) {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_async_connection().await.unwrap();
    for kv in kvs.0.iter() {
        let _: RedisResult<String> = con.get(kv.k.clone()).await;
    }
}

fn bench_wrap(c: &mut Criterion) {
    let mut kvs: Wrapper = Wrapper(Vec::new());
    for _ in 0..10000 {
        let _ = &kvs.0.push(KV {
            k: nanoid!(),
            v: nanoid!(),
        });
    }

    let mut group = c.benchmark_group("redis_big");
    group.sample_size(10);
    group.bench_with_input(BenchmarkId::new("big_set", &kvs), &kvs, |b, kvs| {
        b.to_async(Runtime::new().unwrap()).iter(|| big_set(kvs));
    });

    group.bench_with_input(BenchmarkId::new("big_get", &kvs), &kvs, |b, kvs| {
        b.to_async(Runtime::new().unwrap()).iter(|| big_get(kvs));
    });
}

criterion_group!(benches, bench_wrap);
criterion_main!(benches);
