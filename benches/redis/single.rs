// THESE BENCHMARKS REQUIRE A REDIS SERVER TO ALREADY BE RUNNING

use criterion::BenchmarkId;
use criterion::{criterion_group, criterion_main, Criterion};
use nanoid::nanoid;

use tokio::runtime::Runtime;

use redis::AsyncCommands;
use redis::RedisResult;

struct KV {
    k: String,
    v: String,
}

impl std::fmt::Display for KV {
    fn fmt(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        //write!(f, "{}", self.0)?;
        Ok(())
    }
}

async fn single_set(kv: &KV) {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_async_connection().await.unwrap();
    let _: RedisResult<String> = con.set(kv.k.clone(), kv.v.clone()).await;
    drop(con);
}

async fn single_get(kv: &KV) {
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_async_connection().await.unwrap();
    let _: RedisResult<String> = con.get(kv.k.clone()).await;
    drop(con);
}

fn bench_wrap(c: &mut Criterion) {
    let kv: KV = KV {
        k: nanoid!(),
        v: nanoid!(),
    };

    let mut group = c.benchmark_group("redis_single");
    group.bench_with_input(BenchmarkId::new("single_set", &kv), &kv, |b, kv| {
        b.to_async(Runtime::new().unwrap()).iter(|| single_set(kv));
    });

    group.bench_with_input(BenchmarkId::new("single_get", &kv), &kv, |b, kv| {
        b.to_async(Runtime::new().unwrap()).iter(|| single_get(kv));
    });
}

criterion_group!(benches, bench_wrap);
criterion_main!(benches);
