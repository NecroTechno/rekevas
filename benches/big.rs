// THESE BENCHMARKS REQUIRE A REKEVAS SERVER TO ALREADY BE RUNNING

use criterion::BenchmarkId;
use criterion::{criterion_group, criterion_main, Criterion};
use nanoid::nanoid;

use tokio::runtime::Runtime;

use futures::future::join_all;

struct KV {
    k: String,
    v: String,
}

struct Wrapper(Vec<KV>);

impl std::fmt::Display for Wrapper {
    fn fmt(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        //write!(f, "{}", self.0)?;
        Ok(())
    }
}

async fn big_set(kvs: &Wrapper) {
    let r = rekevas::client::Client::new("127.0.0.1:6380").unwrap();
    let mut fs = Vec::new();
    for kv in kvs.0.iter() {
        fs.push(r.set(kv.k.clone(), kv.v.clone()));
        // r.set(kv.k.clone(), kv.v.clone()).await; force synchronous await - like redis bench
    }
    join_all(fs).await;
    r.end();
}

async fn big_get(kvs: &Wrapper) {
    let r = rekevas::client::Client::new("127.0.0.1:6380").unwrap();
    let mut fs = Vec::new();
    for kv in kvs.0.iter() {
        fs.push(r.get(kv.k.clone()));
        // r.get(kv.k.clone()).await; force synchronous await - like redis bench
    }
    join_all(fs).await;
    r.end();
}

fn bench_wrap(c: &mut Criterion) {
    let mut kvs: Wrapper = Wrapper(Vec::new());
    for _ in 0..10000 {
        let _ = &kvs.0.push(KV {
            k: nanoid!(),
            v: nanoid!(),
        });
    }

    let mut group = c.benchmark_group("big");
    group.sample_size(10);
    group.bench_with_input(BenchmarkId::new("big_set", &kvs), &kvs, |b, kvs| {
        b.to_async(Runtime::new().unwrap()).iter(|| big_set(kvs));
    });

    group.bench_with_input(BenchmarkId::new("big_get", &kvs), &kvs, |b, kvs| {
        b.to_async(Runtime::new().unwrap()).iter(|| big_get(kvs));
    });
}

criterion_group!(benches, bench_wrap);
criterion_main!(benches);
