// THESE BENCHMARKS REQUIRE A REKEVAS SERVER TO ALREADY BE RUNNING

use criterion::BenchmarkId;
use criterion::{criterion_group, criterion_main, Criterion};
use nanoid::nanoid;

use tokio::runtime::Runtime;

struct KV {
    k: String,
    v: String,
}

impl std::fmt::Display for KV {
    fn fmt(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        //write!(f, "{}", self.0)?;
        Ok(())
    }
}

async fn single_set(kv: &KV) {
    let r = rekevas::client::Client::new("127.0.0.1:6380").unwrap();
    let _: Result<String, rekevas::types::RekevasError> = r.set(kv.k.clone(), kv.v.clone()).await;
    r.end();
}

async fn single_get(kv: &KV) {
    let r = rekevas::client::Client::new("127.0.0.1:6380").unwrap();
    let _: Result<String, rekevas::types::RekevasError> = r.get(kv.k.clone()).await;
    r.end();
}

fn bench_wrap(c: &mut Criterion) {
    let kv: KV = KV {
        k: nanoid!(),
        v: nanoid!(),
    };

    let mut group = c.benchmark_group("single");
    group.bench_with_input(BenchmarkId::new("single_set", &kv), &kv, |b, kv| {
        b.to_async(Runtime::new().unwrap()).iter(|| single_set(kv));
    });

    group.bench_with_input(BenchmarkId::new("single_get", &kv), &kv, |b, kv| {
        b.to_async(Runtime::new().unwrap()).iter(|| single_get(kv));
    });
}

criterion_group!(benches, bench_wrap);
criterion_main!(benches);
