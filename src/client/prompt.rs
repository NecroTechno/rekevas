use async_recursion::async_recursion;

use std::io::Write;
use std::process::exit;
use std::string::ToString;

use rekevas::client::Client;

async fn args_req<T: ToString>(args_len: T, r: Client) {
    eprintln!(
        "incorrect number of arguments, {} required",
        args_len.to_string()
    );
    prompt(r).await;
}

#[async_recursion]
pub async fn prompt(r: Client) {
    // read input from prompt
    std::io::stdout().flush().unwrap();
    let mut line = String::new();
    print!("> ");
    std::io::stdout().flush().unwrap();
    std::io::stdin()
        .read_line(&mut line)
        .expect("Error: Could not read a line"); // could probably be handled better...

    // split input into vector
    let args: Vec<&str> = line.trim().split(' ').collect();

    // send signals depending on input match and length
    match args[0] {
        "set" => match args.len() {
            3 => {
                let k = args[1].to_owned();
                let v = args[2].to_owned();
                println!("{:?}", r.set(k, v).await);
                prompt(r).await;
            }
            _ => {
                args_req(3, r).await;
            }
        },

        "get" => match args.len() {
            2 => {
                let k = args[1].to_owned();
                println!("{:?}", r.get(k).await);
                prompt(r).await;
            }
            _ => {
                args_req(2, r).await;
            }
        },

        "del" => match args.len() {
            2..=257 => {
                let mut keys: Vec<String> = Vec::new();
                for k in args.iter().skip(1) {
                    keys.push(k.to_string());
                }
                println!("{:?}", r.del(keys).await);
                prompt(r).await;
            }
            _ => {
                args_req("2-257", r).await;
            }
        },

        "select" => match args.len() {
            2 => {
                let i = args[1].to_owned();
                match i.trim().parse::<u8>() {
                    Ok(i_u8) => {
                        println!("{:?}", r.select(i_u8).await);
                        prompt(r).await;
                    }
                    Err(_e) => {
                        eprintln!("Unable to parse argumet as u8.");
                        prompt(r).await;
                    }
                }
            }
            _ => {
                args_req(2, r).await;
            }
        },

        "dbsize" => {
            println!("{:?}", r.dbsize().await);
            prompt(r).await;
        }

        "flushdb" => {
            println!("{:?}", r.flushdb().await);
            prompt(r).await;
        }

        "exit" => exit(0),
        _ => {
            prompt(r).await;
        }
    }
}
