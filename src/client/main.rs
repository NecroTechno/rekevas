mod prompt;

use crate::prompt::prompt;

use rekevas::client::Client;

#[tokio::main]
async fn main() {
    let r = Client::new("127.0.0.1:6380").unwrap();
    prompt(r).await;
}
