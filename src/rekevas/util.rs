#[macro_export]
macro_rules! extract {
    ($value:expr, $pattern:pat => $extracted_value:expr) => {
        match $value {
            $pattern => Ok($extracted_value),
            ResponseValue::Err(e) => Err(RekevasError::ResponseErr(e)),
            _ => Err(RekevasError::UnexpectedTypeErr(
                "Unexpected type returned from server".to_owned(),
            )),
        }
    };
}
