use serde::{Deserialize, Serialize};

pub type Id = String;

pub trait RekevasResult {}
impl RekevasResult for String {}
impl RekevasResult for u8 {}
impl RekevasResult for usize {}

#[derive(Debug, PartialEq, Eq)]
pub enum RekevasError {
    DecodeErr(String),
    TimeoutErr(String),
    ResponseErr(String),
    UnexpectedTypeErr(String),
    SerializeErr(String),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum CommandValue {
    Set(String, String),
    Get(String),
    Del(Vec<String>),
    Select(u8),
    DBSize,
    FlushDB,
    DecodeErr,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Command {
    pub id: Option<Id>,
    pub cval: CommandValue,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub enum ResponseValue {
    Set(String),
    Get(String),
    Del(u8),
    Select(String),
    DBSize(usize),
    FlushDB(String),
    Err(String),
}

impl ResponseValue {
    pub fn inner_err(&self) -> String {
        match self {
            Self::Err(v) => v.to_owned(),
            _ => "Unable to reach inner ResponseValue error.".to_owned(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct Response {
    pub id: Option<Id>,
    pub cval: ResponseValue,
}

pub enum ServerSignal {
    Persist,
}
