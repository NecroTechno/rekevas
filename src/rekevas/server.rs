use ahash::AHashMap;
use message_io::network::{NetEvent, ResourceId, Transport};
use message_io::node::{self, NodeEvent, NodeTask};

use crate::types::{Command, CommandValue, Response, ResponseValue, ServerSignal};

use std::env;
use std::fs;
use std::io::{Read, Write};
use std::path::Path;

const PERSISTENT_FILENAME: &str = ".rekevaspersist";

pub struct Server {
    _node_task: NodeTask,
}

impl Server {
    pub fn new(port: u16, db_count: u8, persist: bool) -> Server {
        let (handler, listener) = node::split::<ServerSignal>();

        handler
            .network()
            .listen(Transport::FramedTcp, &format!("0.0.0.0:{}", port))
            .unwrap();

        let mut db_map: Vec<AHashMap<String, String>> = Vec::new();
        let mut client_conn_map: AHashMap<ResourceId, u8> = AHashMap::new();

        for _i in 0..db_count {
            db_map.push(AHashMap::new());
        }

        // persist functions should probably should be moved to it's own file
        if persist {
            match env::current_dir()
                .map_err(|e| format!("{}", e))
                .and_then(|mut path| {
                    if Path::exists(&path) {
                        path = path.join(PERSISTENT_FILENAME);
                        Ok(path)
                    } else {
                        Err("Path does not exist".to_string())
                    }
                })
                .and_then(|path| {
                    fs::OpenOptions::new()
                        .read(true)
                        .open(path)
                        .map_err(|e| format!("{}", e))
                })
                .and_then(|mut file| {
                    let mut encoded_db: Vec<u8> = Vec::new();
                    match file.read_to_end(&mut encoded_db) {
                        Ok(_) => Ok(encoded_db),
                        Err(_e) => Err("Unable to read persisted db map".to_string()),
                    }
                })
                .and_then(|encoded_db| {
                    bincode::deserialize(&encoded_db).map_err(|e| format!("{}", e))
                }) {
                Ok(decoded_db) => db_map = decoded_db,
                Err(e) => eprintln!("Unable to restore persisted database: {}", e),
            }
        }

        // Read incoming network events.
        let task = listener.for_each_async(move |event| match event {
            NodeEvent::Network(net_event) => match net_event {
                NetEvent::Connected(_, _) => unreachable!(), // Used for explicit connections.
                NetEvent::Accepted(endpoint, _listener) => {
                    println!("Client connected");
                    client_conn_map.insert(endpoint.resource_id(), 0);
                } // Tcp or Ws
                NetEvent::Message(endpoint, data) => {
                    let decoded: Command = bincode::deserialize(data).unwrap_or(Command {
                        id: None,
                        cval: CommandValue::DecodeErr,
                    });

                    if let Some(db_index) = client_conn_map.get(&endpoint.resource_id()) {
                        if let Some(ref id) = decoded.id {
                            match decoded.cval {
                                CommandValue::Set(k, v) => {
                                    db_map[db_index.to_owned() as usize].insert(k, v);
                                    handler.network().send(
                                        endpoint,
                                        &bincode::serialize(&Response {
                                            id: Some(id.to_owned()),
                                            cval: ResponseValue::Set("OK".to_owned()),
                                        })
                                        .unwrap(),
                                    );
                                    handler.signals().send(ServerSignal::Persist);
                                }
                                CommandValue::Get(k) => {
                                    match db_map[db_index.to_owned() as usize].get(&k) {
                                        Some(v) => {
                                            handler.network().send(
                                                endpoint,
                                                &bincode::serialize(&Response {
                                                    id: Some(id.to_owned()),
                                                    cval: ResponseValue::Get(v.to_owned()),
                                                })
                                                .unwrap(),
                                            );
                                        }
                                        None => {
                                            handler.network().send(
                                                endpoint,
                                                &bincode::serialize(&Response {
                                                    id: Some(id.to_owned()),
                                                    cval: ResponseValue::Err(format!(
                                                        "Key {} does not exist",
                                                        k
                                                    )),
                                                })
                                                .unwrap(),
                                            );
                                        }
                                    }
                                }
                                CommandValue::Del(keys) => {
                                    let mut remove_count: u8 = 0;
                                    for k in keys.iter() {
                                        match db_map[db_index.to_owned() as usize].remove(k) {
                                            Some(_) => {
                                                remove_count += 1;
                                            }
                                            None => {}
                                        }
                                    }
                                    handler.network().send(
                                        endpoint,
                                        &bincode::serialize(&Response {
                                            id: Some(id.to_owned()),
                                            cval: ResponseValue::Del(remove_count),
                                        })
                                        .unwrap(),
                                    );
                                    handler.signals().send(ServerSignal::Persist);
                                }
                                CommandValue::Select(i) => {
                                    if usize::from(i) > db_map.len() - 1 {
                                        handler.network().send(
                                            endpoint,
                                            &bincode::serialize(&Response {
                                                id: Some(id.to_owned()),
                                                cval: ResponseValue::Err(format!(
                                                    "Index {} out of range of valid databases",
                                                    i
                                                )),
                                            })
                                            .unwrap(),
                                        );
                                    } else {
                                        client_conn_map.insert(endpoint.resource_id(), i);
                                        handler.network().send(
                                            endpoint,
                                            &bincode::serialize(&Response {
                                                id: Some(id.to_owned()),
                                                cval: ResponseValue::Select("OK".to_owned()),
                                            })
                                            .unwrap(),
                                        );
                                    }
                                }
                                CommandValue::DBSize => {
                                    handler.network().send(
                                        endpoint,
                                        &bincode::serialize(&Response {
                                            id: Some(id.to_owned()),
                                            cval: ResponseValue::DBSize(
                                                db_map[db_index.to_owned() as usize].len(),
                                            ),
                                        })
                                        .unwrap(),
                                    );
                                }
                                CommandValue::FlushDB => {
                                    db_map[db_index.to_owned() as usize].clear();
                                    handler.network().send(
                                        endpoint,
                                        &bincode::serialize(&Response {
                                            id: Some(id.to_owned()),
                                            cval: ResponseValue::FlushDB("OK".to_owned()),
                                        })
                                        .unwrap(),
                                    );
                                    handler.signals().send(ServerSignal::Persist);
                                }
                                CommandValue::DecodeErr => {
                                    unreachable!(); // DecodeErr cannot have an associated id
                                }
                            }
                        } else {
                            let err_msg = "Server was unable to decode command";
                            handler.network().send(
                                endpoint,
                                &bincode::serialize(&Response {
                                    id: None,
                                    cval: ResponseValue::Err(err_msg.to_owned()),
                                })
                                .unwrap(),
                            );
                            eprintln!("{}", err_msg);
                        }
                    } else {
                        let err_msg = "Server was unable to identify client database connection.";
                        handler.network().send(
                            endpoint,
                            &bincode::serialize(&Response {
                                id: decoded.id,
                                cval: ResponseValue::Err(err_msg.to_owned()),
                            })
                            .unwrap(),
                        );
                        eprintln!("{}", err_msg);
                    }
                }
                NetEvent::Disconnected(endpoint) => {
                    client_conn_map.remove(&endpoint.resource_id());
                    println!("Client disconnected");
                }
            },
            NodeEvent::Signal(signal) => match signal {
                ServerSignal::Persist => {
                    // called from the net events on commands that modify the db map
                    if persist {
                        match env::current_dir()
                            .map_err(|e| format!("{}", e))
                            .and_then(|mut path| {
                                path = path.join(PERSISTENT_FILENAME);
                                fs::OpenOptions::new()
                                    .write(true)
                                    .create(true)
                                    .truncate(true)
                                    .open(path)
                                    .map_err(|e| format!("{}", e))
                            })
                            .and_then(|file| match bincode::serialize(&db_map) {
                                Ok(encoded_db) => Ok((file, encoded_db)),
                                Err(e) => Err(e.to_string()),
                            })
                            .and_then(|(mut file, encoded_db)| {
                                file.write_all(&encoded_db).map_err(|e| format!("{}", e))
                            }) {
                            Ok(_) => {}
                            Err(e) => eprintln!("Unable to persist map: {}", e),
                        }
                    }
                }
            },
        });

        Self {
            _node_task: task,
        }
    }
}
