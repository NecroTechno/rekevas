use clap::Parser;
use rekevas::server::Server;

/// Rekevas Server
#[derive(Parser, Debug)]
#[clap(version, about, long_about = None)]
struct Args {
    #[clap(short, long, value_parser, default_value_t = 6380)]
    port: u16,
    #[clap(short, long, value_parser, default_value_t = 1)]
    db_count: u8,
    #[clap(long, value_parser, default_value_t = false)]
    persist: bool,
}

fn main() {
    let args = Args::parse();
    // let _task = start_task(args.port, args.db_count);
    let _server = Server::new(args.port, args.db_count, args.persist);
}
