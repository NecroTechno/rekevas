// THESE TESTS REQUIRE A REKEVAS SERVER TO ALREADY BE RUNNING

use nanoid::nanoid;

#[tokio::test]
async fn set_and_get() {
    let r = rekevas::client::Client::new("127.0.0.1:6380").unwrap();
    let k = nanoid!();
    let v = nanoid!();
    let res1 = r.set(k.clone(), v.clone()).await;
    let res2 = r.get(k).await;
    r.end();
    assert_eq!(res1, Ok("OK".to_owned()));
    assert_eq!(res2, Ok(v));
}

#[tokio::test]
async fn set_and_del() {
    let r = rekevas::client::Client::new("127.0.0.1:6380").unwrap();
    let k = nanoid!();
    let v = nanoid!();
    let res1 = r.set(k.clone(), v).await;
    let res2 = r.del(vec![k]).await;
    r.end();
    assert_eq!(res1, Ok("OK".to_owned()));
    assert_eq!(res2, Ok(1));
}
